#!/bin/bash
#
# Use this script to create the alerting topics
#

docker-compose exec broker \
  kafka-topics --create --topic payment-transactions \
  --partitions 3 --replication-factor 1 \
  --if-not-exists --zookeeper zookeeper:2181

docker-compose exec broker \
  kafka-topics --create --topic payment-transactionalertsettings \
  --partitions 3 --replication-factor 1 \
  --if-not-exists --zookeeper zookeeper:2181

docker-compose exec broker \
  kafka-topics --create --topic payment-filteredtransactionalertsettings \
  --partitions 3 --replication-factor 1 \
  --if-not-exists --zookeeper zookeeper:2181

docker-compose exec broker \
  kafka-topics --create --topic payment-transactionalerts \
  --partitions 3 --replication-factor 1 \
  --if-not-exists --zookeeper zookeeper:2181
